import React from 'react'
import './Footer.css'

export default () => (
  <div>
    <footer className="footer">
      <div className="container taCenter">
        <span>
          © Copyright {new Date().getFullYear()} Created by{' '}
          <a href="https://www.clockworkbee.co.uk/">CLOCKWORK BEE</a>.
        </span>
      </div>
    </footer>
  </div>
)
