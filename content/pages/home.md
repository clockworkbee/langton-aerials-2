---
template: HomePage
slug: ""
title: Langton Aerials
featuredImage: https://ucarecdn.com/9e7cdf00-63a3-4d15-94f7-73a91ce24c6e/
subtitle: Looking for aerials in Nottingham and the surrounding areas? If so,
  look no further than the experts here at Langton Aerials.
meta:
  description: Langton Aerials, Wifi, Freesat, Freeview, TV, Aerials, CCTV, Radio,
    Wall Mounts
  title: Langton Aerials
---
With over 20 years of experience in the trade, you can count on us to provide a professional and reliable service to customers across the region.

So, for quality aerials and digital TV installation in Nottingham and beyond, call us today.

We offer a wide range of services including Sky and sky in all rooms, Freeview/ freesat installation, extra points, fm and dab aerials, television mounted on walls and set-up.

We also do storm damage and insurance work. We take great pride in the work we do and you can count on us to do a stellar job on your new or existing aerial.

We have been a family run business since 1989 and take pride in looking after our customers.

Whether you need a new digital standard aerial in preparation for the switchover, or you need help and advice about all the different choices you have got for your viewing needs.

All our work is fully guaranteed . Langton Aerials are here for you.

# Services

* **[Freeview](posts/free-view/)**
* **[FreeSat](/posts/free-sat/)**
* **[Radio](/posts/radio/)**
* **[TV Wall Mounts](/posts/tv-wall-mounts/)**
* **[](/posts/tv-wall-mounts/)[CCTV](/posts/cctv/)**
* **[WIFI](/posts/wifi/)**