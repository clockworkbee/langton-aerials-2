---
template: SinglePost
title: Radio
status: Published
date: 2018-03-28
featuredImage: https://ucarecdn.com/cdcc3213-469d-4cf5-a2b4-5a922ba52628/
excerpt: We always carry FM or dad Omnidirectional antennas on the van to help
  improve radio signals in poor areas for all the radio lovers.
categories:
  - category: News
meta:
  description: test meta description
  title: test meta title
---
We always carry FM or dad Omnidirectional antennas on the van to help improve radio signals in poor areas for all the radio lovers.

High gain radio aerials can be arranged if you wish as well as signal boosters available if needed.

We normally mount omnidirectional aerials onto your existing mast so no need for another mast on your roof.

![](https://ucarecdn.com/a5604250-91f8-4bd9-b8b5-00daae0759ab/)

![](https://ucarecdn.com/dad4964a-d467-420e-9368-b829fad1c774/)