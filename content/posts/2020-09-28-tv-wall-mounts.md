---
template: SinglePost
title: TV Wall Mounts
status: Published
date: 2020-09-28
featuredImage: https://ucarecdn.com/a60bec5a-bdee-49a9-8600-d0ed9f3d5f76/
excerpt: >-
  There are different styles and strengths of bracket for wall mounted TVs.


  We need to know if the TV is going to be on a flat wall, for example on your chimney breast or do you want it in the corner of your room or do you need it to have a long reach.
---
![](https://ucarecdn.com/d0574c1d-ead3-4066-a1db-2e9d7f100525/)

There are different styles and strengths of bracket for wall mounted TVs.

We need to know if the TV is going to be on a flat wall, for example on your chimney breast or do you want it in the corner of your room or do you need it to have a long reach.

The type of Mount we use depends on size and weight of your TV and the strength and type of the wall.

Other considerations needed are cable routes, lengths of Scart/HDMI leads for sky boxes or DVD players and where the TV will plug into the electric.

We can give you a price with or without the bracket and always friendly advice you may need help with.

![](https://ucarecdn.com/cddd20a4-a9ad-4ea5-911d-406e91130b6e/ "Flat to Wall Bracket")

![](https://ucarecdn.com/f164442a-9c53-4c23-8c86-a7246ada4fee/ "Long Reach Bracket")

![](https://ucarecdn.com/4613227c-03bb-47fc-83f7-ab7a92d4faec/ "Flat to wall 40 inch bracket")